<?php
    class Home extends Admin_Controller{
        function __construct()
        {
            parent::__construct();
            $this->load->model('action');
        }

        public function index($id)
        {
            $where = array('id' => $id);
            $this->data['users'] = $this->action->read('users',$where);

            $this->load->view("include/header");
            $this->load->view("profile/home",$this->data);
            $this->load->view("include/footer");

        }

        public function update($id){
            $this->data['confirmation'] = null;
            $where = array('id' => $id);
            $this->data['users'] = $this->action->read('users',$where);
            if ($this->input->post('update')){
                $data = array(
                    'name' => $this->input->post('name'),
                    'mobile'=> $this->input->post('mobile'),
                    'email'=> $this->input->post('email'),
                );

                if($this->action->update('users',$data,$where)){
                    $this->data['confirmation'] = "Updated Successfully";
                    redirect("profile/home/index/".$id);
                }

            }

            $this->load->view("include/header");
            $this->load->view("profile/edit",$this->data);
            $this->load->view("include/footer");


        }


    }