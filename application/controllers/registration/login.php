<?php
	class Login extends Admin_Controller{
		function __construct()
		{
			parent::__construct();
            $this->load->model('action');
		}
		function index(){


		    if ($this->input->post('login')){
		        $where = array(
		            'email' => $this->input->post('email'),
                    'pass'  => $this->input->post('pass')
                );
		        $users = $this->action->read('users',$where);
		        if($users){
                    redirect("profile/home/index/".$users[0]->id);
                }
            }

			
			$this->load->view("include/header");
			$this->load->view("login/login");
			$this->load->view("include/footer");
			
		}
		
	}