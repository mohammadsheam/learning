<?php

class SignUp extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('action');
    }

    function index()
    {

        if ($this->input->post('add')) {
            $data = array(
                'date' => date('Y-m-d'),
                'name' => $this->input->post('name'),
                'mobile'=> $this->input->post('mobile'),
                'email'=> $this->input->post('email'),
                'pass' => $this->input->post('pass')
            );

            if ($this->action->add('users', $data)) {
                $this->session->set_flashdata('confirmation', 'Successfully Added');
                redirect('registration/asdasd','refresh');
            }


        }


        $this->load->view("include/header");
        $this->load->view("signup/add",$this->data);
        $this->load->view("include/footer");

    }

}