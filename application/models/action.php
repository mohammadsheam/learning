<?php
    class Action extends My_Model{
        function __construct() {
            parent::__construct();
        }

        //insert into database
        public function add($table, $data){
            $this->db->insert($table, $data);
            //return true;
        }

        // retrieve from database
        public function read($table, $where = array()) {
            if(count($where) > 0){
                $query = $this->db->get_where($table,$where);
                return $query->result();
            } else {
                $query = $this->db->get($table);
                return $query->result();
            }
        }

        //Update Data into database
        public function update($table,$data,$where = array()){
            $this->db->set($data);
            $this->db->where($where);
            $this->db->update($table);

        }


    }