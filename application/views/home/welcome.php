<div class="container" ng-app="ngApp">

    <div class="col-md-6" ng-controller="math">
        <p class="text-center" style="font-size: large">Angular</p>
        <label>Number 1:</label>
        <input type="number" ng-model="number1" class="form-control">
        <label>Number 2:</label>
        <input type="number" ng-model="number2" class="form-control">
        <p class="text-primary" style="margin-top: 5px;">Result: {{ number1+number2 }}</p>
    </div>

    <div class="col-md-6" ng-controller="todo">

        <div class="col-md-8">
            <input style="margin-top: 25px;" type="text" class="form-control" ng-model="task">
        </div>
        <div class="col-md-4">
            <a ng-click="addTask()" style="margin-top: 15px;" href="#" class="btn btn-info btn-lg">
                <span class="glyphicon glyphicon-plus"></span>
            </a>
        </div>
        <li ng-repeat="item in allTask">{{ item.task }}</li>
    </div>

</div>


<div class="container">
    <div class="col-md-6" id="vApp">
        <p class="text-center" style="font-size: large">Vue</p>
        <label>Number 1:</label>
        <input type="number" v-model.number="number1" class="form-control">
        <label>Number 2:</label>
        <input type="number" v-model.number="number2" class="form-control">
        <p class="text-primary" style="margin-top: 5px;">Result: {{ number1+number2 }}</p>
    </div>
</div>


<div class="container">
    <div class="col-md-12">
        <a class="btn btn-primary" href="<?php echo site_url("registration/signup"); ?>">Signup</a>
        <a class="btn btn-success" href="<?php echo site_url("registration/login"); ?>">Login</a>
    </div>
</div>