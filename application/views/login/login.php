<style>
    .req{
        color: #ff7835;
    }
</style>

<div class="panel-body">

    <?php
    $attr=array("class"=>"form-horizontal");
    echo form_open_multipart('', $attr);
    ?>



    <div class="form-group">
        <label class="col-md-2 control-label">Email <span class="req">*</span></label>
        <div class="col-md-4">
            <input type="email" name="email" class="form-control" required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Password </label>
        <div class="col-md-4">
            <input type="password" name="pass" class="form-control" required>
        </div>
    </div>


    <div class="col-md-6">

        <div class="btn-group pull-right">
            <input type="submit" name="login" value="Login" class="btn btn-primary">
            <input style="margin-left: 10px;" type="reset" name="reset" value="Reset" class="btn btn-danger">
        </div>
    </div>

    <?php echo form_close(); ?>


</div>

<div class="col-md-6">
    <div class="btn-group">
        <a href="<?php echo site_url("registration/signup");?>" class="btn btn-warning">Signup</a>
    </div>
</div>