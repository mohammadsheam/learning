
<div class="container">

    <?php echo $confirmation; ?>

    <?php
    $attr=array("class"=>"form-horizontal");
    echo form_open('', $attr);
    ?>

    <div class="form-group">
        <label class="col-md-2 control-label">Name</label>
        <div class="col-md-4">
            <input type="text" name="name" value="<?php echo $users[0]->name;?>" class="form-control" >
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Email </label>
        <div class="col-md-4">
            <input type="email" value="<?php echo $users[0]->email;?>" name="email" class="form-control" >
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Mobile </label>
        <div class="col-md-4">
            <input type="text" name="mobile" value="<?php echo $users[0]->mobile;?>"  class="form-control" >
        </div>
    </div>

    <div class="col-md-6">
        <div class="btn-group pull-right">
            <input type="submit" name="update" value="Update" class="btn btn-primary">
        </div>
    </div>

    <?php echo form_close(); ?>

</div>

