<style>
	.req{
		color: #ff7835;
	}
</style>

<div class="container">

    <?php echo $this->session->flashdata('confirmation'); ?>
	
	<?php
		$attr=array("class"=>"form-horizontal");
		echo form_open_multipart('', $attr);
	?>

	<div class="form-group">
		<label class="col-md-2 control-label">Name <span class="req">*</span></label>
		<div class="col-md-4">
			<input type="text" name="name" class="form-control" required>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-2 control-label">Email <span class="req">*</span></label>
		<div class="col-md-4">
			<input type="email" name="email" class="form-control" required>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-2 control-label">Mobile </label>
		<div class="col-md-4">
			<input type="text" name="mobile" class="form-control" >
		</div>
	</div>
    <div class="form-group">
        <label class="col-md-2 control-label">Password <span class="req">*</span> </label>
        <div class="col-md-4">
            <input type="password" name="pass" class="form-control" required >
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Confirm Password <span class="req">*</span> </label>
        <div class="col-md-4">
            <input type="password" class="form-control" required>
        </div>
    </div>

	
	
	<div class="col-md-6">
		<div class="btn-group pull-right">
			<input type="submit" name="add" value="Save" class="btn btn-primary">
			<input style="margin-left: 10px;" type="reset" name="reset" value="Reset" class="btn btn-danger">
		</div>
	</div>
	
	<?php echo form_close(); ?>



</div>

<div class="col-md-6">
    <div class="btn-group">
        <a href="<?php echo site_url("registration/login");?>" class="btn btn-warning">Login</a>
    </div>
</div>
